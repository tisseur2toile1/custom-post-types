<?php

namespace T2T\CustomPostTypes;

// Set up plugin class
class Init {

  function __construct() {

    // Include all post types
    foreach (glob(CPT_PLUGIN_DIR . "app/types/*.php") as $filename) {
      include $filename;
    }
  }
}
