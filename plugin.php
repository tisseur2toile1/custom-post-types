<?php
/*
Plugin Name: Custom Post Types
Plugin URI: https://gitlab.com/tisseur2toile1/custom-post-types
Description: Custom Post Types
Version: 1.0
Author: jeFFF @ Tisseur de Toile
Author URI: https://www.tisseur-de-toile.fr
License: MIT
*/

namespace T2T\CustomPostTypes;


//Set up autoloader
require __DIR__ . '/vendor/autoload.php';

//Define Constants
define( 'CPT_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'CPT_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

// Autoload the Init class
$example_init = new Init();
